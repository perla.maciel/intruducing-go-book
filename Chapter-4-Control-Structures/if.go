package main

import "fmt"

func main() {
	/*Ejemplo de la sentencia if con numeros pares e impares
	usando la sentencia for para recorrer los numeros del 1 al 10
	*/
	for i := 1; i <= 10; i++ {
		if i % 2 == 0 {
			fmt.Println(i, "even")
		} else {
			fmt.Println(i, "odd")
		}
	}
}