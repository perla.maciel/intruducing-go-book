package main

import "fmt"

func main() {
	/*Este ciclo comienza en el 1 y aumenta de 
	uno en uno hasta llegar al 100
	*/
	for i := 1; i <= 100; i++ {
		if (i%3) == 0 && (i%5) == 0 {
			fmt.Println("FizzBuzz")
		}else if (i%3) == 0 {
			fmt.Println("Fizz")
		}else if (i%5) == 0 {
			fmt.Println("Buzz")
		}else{
			fmt.Println(i)
		}
	}
}