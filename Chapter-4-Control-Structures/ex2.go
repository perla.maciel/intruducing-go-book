package main

import "fmt"

func main() {
	/*Este ciclo comienza en el 1 y aumenta de 
	uno en uno hasta llegar al 100
	*/
	for i := 1; i <= 100; i++ {
		if (i%3) == 0 {
			fmt.Print(i," ")
		}
	}
}