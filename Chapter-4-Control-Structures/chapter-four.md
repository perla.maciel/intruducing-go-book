# Capítulo 4. Estructuras de Control

Este capítulo abarca las estructuras de control, siendo estas las sentencias **for**, **if** y **switch**. 

Algunos ejemplos de cada uno:
* [Sentencia **for**](../Chapter-4-Control-Structures/for.go).
* [Sentencia **if**](../Chapter-4-Control-Structures/if.go).
* [Sentencia **switch**](../Chapter-4-Control-Structures/switch.go).

## Ejercicios
### 1. ¿Qué imprime el siguiente programa?

```go 
	i := 10
	if i > 10 {
		fmt.Println("Big")
	} else {
		fmt.Println("Small")
	}
```
Ya que i es igual a 10 y la condición dice si es mayor a 10, entonces entra al **else** imprime "Small", como se muetra a continuación:
```Output
	Small
```
Ver [código](../Chapter-4-Control-Structures/ex1.go).

### 2. Escribe un programa que imprima todos los números entre el 1 y el 100 que son divisibles por 3 (i.e. 3,6,9,etc.).

```go 
	for i := 1; i <= 100; i++ {
		if (i%3) == 0 {
			fmt.Print(i," ")
		}
	}
```
El resultado en terminal es el siguiente:
```Output
	3 6 9 12 15 18 21 24 27 30 33 36 39 42 45 48 51 54 57 60 63 66 69 72 75 78 81 84 87 90 93 96 99
```
Ver [código](../Chapter-4-Control-Structures/ex2.go).

### 3. Escribe un programa que imprima los 

```go 
	for i := 1; i <= 100; i++ {
		if (i%3) == 0 && (i%5) == 0 {
			fmt.Println("FizzBuzz")
		}else if (i%3) == 0 {
			fmt.Println("Fizz")
		}else if (i%5) == 0 {
			fmt.Println("Buzz")
		}else{
			fmt.Println(i)
		}
	}
```

El resultado en terminal es el siguiente:
```Output
	1
	2
	Fizz
	4
	Buzz
	Fizz
	7
	8
	Fizz
	Buzz
	11
	Fizz
	13
	14
	FizzBuzz
	16
	17
	Fizz
	19
	Buzz
	Fizz
	22
	23
	Fizz
	Buzz
	26
	Fizz
	28
	29
	FizzBuzz
	31
	32
	Fizz
	34
	Buzz
	Fizz
	37
	38
	Fizz
	Buzz
	41
	Fizz
	43
	44
	FizzBuzz
	46
	47
	Fizz
	49
	Buzz
	Fizz
	52
	53
	Fizz
	Buzz
	56
	Fizz
	58
	59
	FizzBuzz
	61
	62
	Fizz
	64
	Buzz
	Fizz
	67
	68
	Fizz
	Buzz
	71
	Fizz
	73
	74
	FizzBuzz
	76
	77
	Fizz
	79
	Buzz
	Fizz
	82
	83
	Fizz
	Buzz
	86
	Fizz
	88
	89
	FizzBuzz
	91
	92
	Fizz
	94
	Buzz
	Fizz
	97
	98
	Fizz
	Buzz
```
Ver [código](../Chapter-4-Control-Structures/ex3.go).

[ :arrow_backward: Capítulo Anterior](/Chapter-3-Variables/chapter-three.md) - [Capítulo Siguiente :arrow_forward: ](/Chapter-5-Arrays-Slices-Maps/chapter-five.md)