package main

import "fmt"

func main() {
	/*Ejercicio 1 */
	xs := []float64{98,93,77,82,83}
	fmt.Println(Average(xs))
	//Prueba con array vacío	
	xst := []float64{}
	fmt.Println(Average(xst))
}

func Average(xs []float64) float64 {
	if len(xs) == 0 {
		return 0
	}else {
		total := 0.0
		for _, v := range xs {
			total += v
		}
		return total / float64(len(xs))
	}
}