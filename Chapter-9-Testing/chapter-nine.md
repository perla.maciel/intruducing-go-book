# Capítulo 9. Testing

Este capítulo abarca testing de código, el lenguaje Go incluye un programa que se ejecuta con de la siguiente manera:
```go 
	go test 
``` 
Y lo que muestra de salida es lo siguiente:
```go 
	PASS
	ok nombre-del-programa 0.032s	
```

Ejemplo para usar este programa:
* [**Testing**](/Chapter-9-Testing/ex-test.go).

## Ejercicios
### 1. Escribir un buen conjunto de pruebas no siempre es fácil, pero el proceso de escribirlas a menudo revela más sobre un problema de lo que te das cuenta al prnicipio. Por ejemplo, con nuestra función *Average* del [Capítulo 6](/Chapter-6-Functions/func.go), ¿qué sucede si pasas una lista vacía *([]float64{})* ?¿Cómo podría ser modificada la función para retornar 0 en este caso?

```go 
	func average(xs []float64) float64 {
		if len(xs) == 0 {
			return 0
		}else {
			total := 0.0
			for _, v := range xs {
				total += v
			}
			return total / float64(len(xs))
		}
	}
```
Ver [código](/Chapter-9-Testing/ex1.go).

### 2. Escribe una serie de pruebas para las funcinoes *Min* y *Max* que escribiste en eĺ capítulo anterior.

```go 
	package math
	import "testing"

	func TestMath(t *testing.T) {
	    cases := []struct {
	        xs []float64
	        max, min float64
	    }{
	        {
	            xs: []float64{3, 5, 2, 1, 7, 9},
	            max: 9,
	            min: 1,
	        },
	        {
	            xs: []float64{},
	            max: 0,
	            min: 0,
	        },
	    }

	    for _, c := range cases {
	        max := Max(c.xs)
	        if max != c.max {
	            t.Errorf("expected %f got %f", c.max, max)
	        }
	        min := Min(c.xs)
	        if min != c.min {
	            t.Errorf("expected %f got %f", c.min, min)
	        }
	    }
	}
```
Ver [código](/Chapter-9-Testing/ex2.go).


[ :arrow_backward: Capítulo Anterior](/Chapter-8-Packages/chapter-eight.md) - [Capítulo Siguiente :arrow_forward: ](/Chapter-10-Concurrency/chapter-ten.md)