# Capítulo 5. Arrays, Slices y Maps

Este capítulo abarca otro tipo de datos **arrays**, **slices** y **maps**. 

Algunos ejemplos de cada uno:
* [Sentencia **arrays**](../Chapter-5-Arrays-Slices-Maps/array.go).
* [Sentencia **slices**](../Chapter-5-Arrays-Slices-Maps/slice.go).
* [Sentencia **maps**](../Chapter-5-Arrays-Slices-Maps/map.go).

## Ejercicios
### 1. ¿Cómo accesas al cuarto elemento de un array o slice?

```go 
	arr[3] || sl[3]	
```

### 2. ¿Cuál es la longitud de un slice creado usando make([]int,3,9)?

```Output 
	3	
```

### 3. Dado el siguiente array, ¿qué resultado daría **x[2:5]**?
```go 
	x := [6]string{"a","b","c","d","e","f"}
```
El resultado sería:
```Output
	cde	
```

### 4. Escribe un programa que encuentre el número más pequeño de la siguiente lista:
```go 
	x := []int{
		48,96,86,68,
		57,82,63,70,
		37,34,83,27,
		19,97, 9,17,
	}	
```

```go 
	var min int
	min = x[0] 
	for i := 0; i < len(x); i++ {
		if x[i] < min {
			min = x[i]
		}
	}
	fmt.Println(min)
```
El resultado en terminal sería:
```Output
	9	
```

Ver [código](../Chapter-5-Arrays-Slices-Maps/ex4.go).

[ :arrow_backward: Capítulo Anterior](/Chapter-4-Control-Structures/chapter-four.md) - [Capítulo Siguiente :arrow_forward: ](/Chapter-6-Functions/chapter-six.md)