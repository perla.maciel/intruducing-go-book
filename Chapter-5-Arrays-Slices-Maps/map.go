package main

import "fmt"

func main() {
	/*Ejemplo de la declaración de un mapa
	indexado por un string.

	Existen multiples formas de realizar la
	declaración.
	*/
	elements := make(map[string]string)
	elements["H"] = "Hydrogen"
	elements["He"] = "Helium"
	elements["Li"] = "Lithium"
	elements["Be"] = "Beryllium"
	elements["B"] = "Boron"
	elements["C"] = "Carbon"
	elements["N"] = "Nitrogen"
	elements["O"] = "Oxygen"
	elements["F"] = "Fluorine"
	elements["Ne"] = "Neon"

	if name, ok := elements["Un"]; ok {
		fmt.Println(name, ok)
	}

	/*Otra forma más corta de declarla es como se 
	declara un array*/
	elements2 := map[string]string{
		"H": "Hydrogen",
		"He": "Helium",
		"Li": "Lithium",
		"Be": "Beryllium",
		"B": "Boron",
		"C": "Carbon",
		"N": "Nitrogen",
		"O": "Oxygen",
		"F": "Fluorine",
		"Ne": "Neon",
	}
	
	if nom, ok := elements2["Un"]; ok {
		fmt.Println(nom, ok)
	}

	/*Y es posible almacenar multiple información
	de un mismo elemento.
	*/
	elements3 := map[string]map[string]string{
		"H": map[string]string{
			"name":"Hydrogen",
			"state":"gas",
		},
		"He": map[string]string{
			"name":"Helium",
			"state":"gas",
		},
		"Li": map[string]string{
			"name":"Lithium",
			"state":"solid",
		},
		"Be": map[string]string{
			"name":"Beryllium",
			"state":"solid",
		},
		"B": map[string]string{
			"name":"Boron",
			"state":"solid",
		},
		"C": map[string]string{
			"name":"Carbon",
			"state":"solid",
		},
		"N": map[string]string{
			"name":"Nitrogen",
			"state":"gas",
		},
		"O": map[string]string{
			"name":"Oxygen",
			"state":"gas",
		},
		"F": map[string]string{
			"name":"Fluorine",
			"state":"gas",
		},
		"Ne": map[string]string{
			"name":"Neon",
			"state":"gas",
		},
	}
	if el, ok := elements3["Li"]; ok {
		fmt.Println(el["name"], el["state"])
	}
}