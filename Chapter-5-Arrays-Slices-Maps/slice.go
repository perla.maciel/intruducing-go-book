package main

import "fmt"

func main() {
	/*Ejemplo de un slice en el que tenemos
	un array y tomamos un segmento de este.
	*/
	arr := [5]float64{1,2,3,4,5}
	/*Existen varias formas de declarar un slice
	donde 5 es el tamaño del slice y 10 es el tamaño máximo 
	del array original.*/
	x := make([]float64, 5, 10)
	//El [0:3] representa el fragmento que que se desea
	x1 := arr[0:3]
	fmt.Println(x[2:5],x1)
}