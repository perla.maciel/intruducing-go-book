package main

import "fmt"

func main() {
	/*Ejemplo de la declaración y uso de un array con 
	la suma de varias calificaciones y obtención del
	promedio*/
	var x [5]float64
	x[0] = 98
	x[1] = 93
	x[2] = 77
	x[3] = 82
	x[4] = 83
	
	//Otra forma de declarar la variable sería la siguiente
	//x1 := [5]float64{ 98, 93, 77, 82, 83 }
	/*o si es demasiado larga tambíen se puede escribir de
	de la siguiente manera*/
	x2 := [5]float64{
		98,
		93,
		77,
		82,
		83,
	}	

	var total float64 = 0
	for i := 0; i < len(x2); i++ {
		total += x2[i]
	}
	fmt.Println(total / float64(len(x2)))

}