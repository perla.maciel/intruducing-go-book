package main

import "fmt"

func main() {
	fmt.Print("Enter a number in ft: ")
	var ft float64
	const ft_m = 0.3048
	fmt.Scanf("%f", &ft)
	m :=  ft * ft_m
	fmt.Println(m)
}