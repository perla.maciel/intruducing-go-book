# Capítulo 3. Variables

Este capítulo abarca la forma de declarar variables en Go. 

Se realizó un [programa de ejemplo](../Chapter-3-Variables/example-program.go) que muestra las formas de declarar variables.

## Ejercicios
### 1. ¿Cuáles son las dos formas de crear una nueva variable?

Para declarar variables en Go tenemos dos
formas:

* Con **var** + **nombre de la variable** + **tipo de dato**
```go 
	var Newvar int	
```

* Con  **nombre de la variable** + **:=** + **valor deseado a guardar** 
```go 
	Newvar := 123456	
```

### 2. ¿Cuál es el valor de *x* después de ejecutar *x := 5; x +=1*?

```go 
	x := 5
	x +=1	
	fmt.Println(x)
```

```Output
	6	
```
Ver [código](../Chapter-3-Variables/ex2.go).

### 3. ¿Qué es *Scope*? ¿Cómo se determina el *scope* de una variable en Go?

Es el alcance que tiene una variable, dependiendo del lugar en que se declare será su alcance. Es decir, si es declarado dentro de una función, sólo será vista dentro de dicha función; en cambio si se declara fuera de la función se puede ver como una variable global capas de ser usada por cualquier función.


### 4. ¿Cuál es la diferencia entre *var* y *const*?

La diferencia reside en el cambio del valor asignado, mientras que la variable declarada con **var** puede ser modificada, la declarada con **const** no puede ser alterada. 


### 5. Usando el [programa de ejemplo](../Chapter-3-Variables/example-program.go) como un punto de inicio, escribe un programa que convierta de Fahrenheit a Celsius *(C=(F-32) * 5/9)*.

```go 
	fmt.Print("Enter temperature: ")
	var F float64
	fmt.Scanf("%f", &F)
	C := (F-32) * 5/9
	fmt.Println(C)
```

Ver [código](../Chapter-3-Variables/ex5.go).

### 6. Escribe otro programa que convierta de pies a metros *(1 ft = 0.3048 m)*.

```go
	fmt.Print("Enter a number in ft: ")
	var ft float64
	const ft_m = 0.3048
	fmt.Scanf("%f", &ft)
	m :=  ft * ft_m
	fmt.Println(m)
```
Ver [código](../Chapter-3-Variables/ex6.go).

[ :arrow_backward: Capítulo Anterior](/Chapter-2-Types/chapter-otwo.md) - [Capítulo Siguiente :arrow_forward: ](/Chapter-4-Control-Structures/chapter-four.md)