package main

import "fmt"

func main() {
	//Ejercicio 4 de Paquetes
	xs := []float64{98,93,77,82,83}
	fmt.Println(Min(xs))
	fmt.Println(Max(xs))
}

/*Funcion que retorna el minimo*/
func Min(xs []float64) float64 {
	min := xs[0]
	for _, v := range xs {
		if v < min {
			min = v
		}
	}
	return min
}

/*Funcion que retorna el maximo*/
func Max(xs []float64) float64 {
	max := 0.0
	for _, v := range xs {
		if v > max {
			max = v
		}
	}
	return max
}