# Capítulo 8. Paquetes

Este capítulo abarca el uso de paquetes dentro del código. 

## Ejercicios
### 1. ¿Por qué usamos paquetes?

Para hacer progrmas grandes en unos más pequeños, que son más sencillos de entender y de mantener; además ayuda a reusar el código. 

### 2. ¿Cuál es la diferencia entre un identificador que empieza con una letra mayúscula con una que no(e.g. Average versus average)?

Cuando empiezan con una letra mayúscula pueden exportados, es decir pueden ser accesados por otros paquetes. Por otro lado, si comienzan con una letra minúscula no.

### 3. ¿Qué es un alias de paquete? ¿Cómo se hace uno?

El alias de un paquete es un nombre alternativo que se le da a un paquete especificado cuando importas dicho paquete. La sentencia se vería de la siguiente manera:

```go 
	package f "fmt"	
```

### 4. Copiamos la funcion *average* del [Capítulo 6](/Chapter-6-Functions/func.go) a nuestro nuevo paquete. Crea las funciones *Min* y *Max* que encuentran el valor mínimo y máximo en un slice de float64.  

```go 
	/*Funcion que retorna el minimo*/
	func Min(xs []float64) float64 {
		min := xs[0]
		for _, v := range xs {
			if v < min {
				min = v
			}
		}
		return min
	}

	/*Funcion que retorna el maximo*/
	func Max(xs []float64) float64 {
		max := 0.0
		for _, v := range xs {
			if v > max {
				max = v
			}
		}
		return max
	}
```
Ver [código](/Chapter-8-Packages/ex4.go).

### 5. ¿Cómo documentarías la función creada en el ejercicio 4?

Para documentar las funciones se agregarían comentarios antes de las funciones, estos comentarios se guardarían como documentación en la herramienta *godoc*.

[ :arrow_backward: Capítulo Anterior](/Chapter-7-Structs-Interfaces/chapter-seven.md) - [Capítulo Siguiente :arrow_forward: ](/Chapter-9-Testing/chapter-nine.md)
