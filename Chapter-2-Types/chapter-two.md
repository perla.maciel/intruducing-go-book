# Capítulo 2. Tipos

Este capítulo abarca los diferentes tipos de datos que se pueden manejar, en el capítulo anterior se hizo uso del tipo string.

## Ejercicios
### 1. ¿Cómo son almacenados los enteros en una computadora?

 Los numeros enteros son almacenados en el sistema binario, y se pueden definir el numero de bits que se requieran,  llendo del 8 hasta 64 bits. 

### 2. Sabemos que (en base 10) el numero más grande de un dígito es 9 y el más grande de dos dígitos es 99. Dado eso en binario el numero más grande de dos dígitos es 11 (3), el más grande de tres dígitos es 111 (7) y el más grande de cuatro dígitos 1111 (15), ¿Cuál es el más grande de ocho dígitos?  

En base 10 el resultado sería 99999999, y en base 2 sería 11111111 = 255 en base 10.

### 3. Aunque puede ser dominado por la tarea, puedes usar Go como una calculadora. Escribe un programa que compute 32,132 x 42,453 e imprime el resultado en la terminal (usa el operador de multiplicación * )    

```go 
	package main

	import "fmt"

	func main(){
		fmt.Println(32132,"*",42453,"=",32132*42453)
	}	
```
Resultado en terminal:

```Output
	32132 * 42453 = 1364099796	
```
Ver [código](../Chapter-2-Types/ex3.go).

### 4. ¿Qué es un String? ¿Cómo encuentras su longitud? 

Un string es una cadena de caracteres.

Su longitud puede ser encontrada usando una función llamada *len(parametro)* donde *parametro* es la cadena de caracteres. 

```go 
	fmt.Println(len("Cadena de caracteres"))	
```

Mostrando en terminal lo siguiente:

```Output
	20	
```
Ver [código](../Chapter-2-Types/ex4.go).

### 5. ¿Cuál es el valor de la expresión (true && false) || (false && true) || !(false && false)?

```go 
	fmt.Prinln((true && false) || (false && true) || !(false && false))	
```

Resultado en terminal:

```Output
	true	
```
Ver [código](../Chapter-2-Types/ex5.go).


 [ :arrow_backward: Capítulo Anterior](/Chapter-1-Getting-Started/chapter-one.md) - [Capítulo Siguiente :arrow_forward: ](/Chapter-3-Variables/chapter-three.md)	