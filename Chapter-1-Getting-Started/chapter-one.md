# Capítulo 1. Empezando

Este capítulo abarca la instalación y ejecución de Go, además de crear nuestro primer programa en Go.

## Ejercicios
 ### 1. ¿Qué es un espacio en blanco?

Es el espacio entre caracteres (tabilaciones, espacios y saltos de línea).

 ### 2. ¿Qué es un comentario? ¿Cuáles son las dos formas de escribir un comentario?

Son lineas dentro del código que son ignoradas por el compilador, y usalmente son utilizadas por el programador para hacer notas sobre el código.

Existen dos formas de realizar comentarios:

* Por línea
```go
	//Este es un comentario por línea
```

* Por bloque de líneas
```go 
	/*Este es un comentario
	por bloque de líneas */	
``` 
 
 ### 3. Nuestro programa comienza con **package main**. ¿Cómo comenzarían los archivos en la paqueteria *fmt*?

```go 
	package fmt	
``` 

 ### 4. Usamos la función *Println* definida en la paqueteria *fmt*. Si se desea usar le función *Exit* de la paquetería *os*, ¿qué se necesitaría hacer?

```go 
	import "os"
	//más líneas de codigo
	os.Exit()	
``` 

 ### 5. Modifica el [primer programa](main.go), en donde en lugar de imprimir *Hello, World* imprima *Hello, my name is* seguido de nuestro nombre.

```go 
	package main 

	import "fmt"

	func main (){
	    fmt.Println("Hello, my name is Perla.")
	}	
``` 

 Ver [programa hello, my name is...](ex5.go)


 [Siguiente Capítulo :arrow_forward: ](/Chapter-2-Types/chapter-two.md)	