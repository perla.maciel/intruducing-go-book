# Capítulo 10. Concurrencia

Los programas grandes a menudo se componen de muchos subprogramas más pequeños. Por ejemplo, un servidor web maneja las solicitudes realizadas desde navegadores web y sirve páginas web HTML en respuesta. Cada solicitud se maneja como un pequeño programa.
Sería ideal que los programas como estos puedan ejecutar sus componentes más pequeños al mismo tiempo (en el caso del servidor web, para manejar múltiples solicitudes). Avanzar en más de una tarea simultáneamente se conoce como *concurrencia*. Go tiene un amplio soporte para la concurrencia usando goroutines y canales. 

Ejemplo:
* [**Goroutines** y **Channels**](../Chapter-10-Concurrency/ex-prog.go).


## Ejercicios
### 1. ¿Cómo específicas la dirección de un tipo channel? 

Para específicar la dirección de un canal podemos hacerlo de las siguientes maneras:
* receive-only
```go 
	<-
```
o
```go 
	<-chan int
```
* send-only
```go 
	chan<- int
```

### 2. Escribe tu propia función *Sleep* usando *time.After*.

```go 
	func Sleep(duration time.Duration) {
		<-time.After(duration)
	}
```

### 3. ¿Qué es un buffered channel?¿Cómo crearías uno con capacidad de 20?

Un buffered channel envía o recibe un mensaje sin esperar, es decir, es asincrono; solo si el canal esta lleno el envío esperará hasta que haya algún lugar para por lo menos un *int*.

Y para crear un buffered channel con una capacidad de 20 se hace de la siguiente manera:
```go 
	c := make(chan int, 20)
```

[ :arrow_backward: Capítulo Anterior](/Chapter-9-Testing/chapter-nine.md)