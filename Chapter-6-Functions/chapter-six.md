# Capítulo 6. Funciones

Este capítulo abarca la declaración de funciones; desde la simple declaración de una función que retorna un tipo de dato hasta funciones dentro de funciones, además de el manejo de recursión, las funciones panic,defer,recover y el uso de punteros. 

Algunos ejemplos:
* [**Funciones**](../Chapter-6-Functions/func.go).

## Ejercicios
### 1. *sum* es una función que toma un slice de números y los agrega juntos. ¿Cómo se vería esta función en Go?

```go 
	func sum(n []int)int{
		total:=0
	    for i := 0; i < length(xs); i++ {
		    total = total + xs[i]
	    }
	    return total
	}	
```

### 2. Escribe una función que tome un entero, le saque la mitad y retorne *true* si es par o *false* si es impar. Por ejemplo, half(1) debería retornar (0, false) y half(2) debería retornar (1, true)

```go 
	func half(n int) (n1 int, b bool){
		return n%2, n%2 == 0
	}	
```
Ver [código](../Chapter-6-Functions/ex2.go).

### 3. Escribe una función que reciba varios enteros como parametro (*variadic*) que encuentre el número mayor de una lista de números.

```go 
	func greater(n ...int) int {
		max := 0
		for i:=0 ; i < len(n) ; i++ {
			if n[i] > max {
				max = n[i]
			}
		} 
		return max
	} 	
```
Ver [código](../Chapter-6-Functions/ex3.go).

### 4. Usando *makeEvenGenerator* como un ejemplo, escribe una función *makeOddGenerator* que genere numeros impares.

```go 
	func makeOddGenerator() func() uint {
		i := uint(1)
		return func() (ret uint) {
			ret = i
			i += 2
			return
		}
	}	
```
Ver [código](../Chapter-6-Functions/ex4.go).

### 5. La serie de Fibonacci es definida como: *fib(0) = 0 , fib(1) = 1 , fib(n) = fib(n-1) + fib(n-2)*. Escribe una función recursiva que pueda encontrar *fib(n)*

```go 
	func fib(n int) int{
		if n == 0 {
			return 0
		}else if n == 1 {
			return 1
		}else{
			return fib(n-1) + fib(n-2)
		}
	}	
```
Ver [código](../Chapter-6-Functions/ex5.go).

### 6. ¿Qué son *defer, panic y recover*?¿Cómo te recuperas de un *runtime panic*?

Cada una tiene un objetivo diferente:
* *defer* agenda la función llamada hasta finalizar la ejecución de la función.
* *panic* crea un error en *runtime*.
* *recover* permite recuperarse de un error en runtime.

Utilizando la funcion *recover* dentro de la función *defer*, para ejecución al final y retorna el valor que fue pasado a la llamada de *panic*.

```go 
	defer func() {
		str := recover()
		fmt.Println(str)
	}()
	panic("PANIC")	
```

### 7. ¿Cómo obtines la dirección de memoria de una variable?

Para obtener la dirección de memoria de una variable se hace uso del operador & seguido de la variable deseada.
```go 
	dir_memoria := &x	
```

### 8. ¿Cómo asignas un valor a un puntero?

```go 
	*Puntero = 9	
```

### 9. ¿Cómo creas un nuevo puntero?

```go 
	newPuntero := new(int)	
```

### 10. ¿Cuál es el valor de *x* después de ejecutar el siguiente programa?

```go 
	func square(x *float64) {
		*x = *x * *x
	}
	func main() {
		x := 1.5
		square(&x)
	}
```
El resultado sería el siguiente:
```Output 
	2.25
```
Ver [código](../Chapter-6-Functions/ex10.go).

### 11. Escribe un programa que pueda intercambiar dos enteros *( x := 1; y := 2; swap(&x, &y) debería de resultar en x=2 and y=1 )*.

```go 
	func swap(x *int, y *int){
		aux := *x
		*x = *y
		*y = aux
	}	
```
Ver [código](../Chapter-6-Functions/ex11.go).


[ :arrow_backward: Capítulo Anterior](/Chapter-5-Arrays-Slices-Maps/chapter-five.md) - [Capítulo Siguiente :arrow_forward: ](/Chapter-7-Structs-Interfaces/chapter-seven.md)