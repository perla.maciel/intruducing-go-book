package main

import "fmt"

func main() {
	/*Ejercicio 2 */
	x := 1; 
	y := 2; 
	fmt.Println(x,y)
	swap(&x, &y)
	fmt.Println(x,y)	
}

func swap(x *int, y *int){
	aux := *x
	*x = *y
	*y = aux
}