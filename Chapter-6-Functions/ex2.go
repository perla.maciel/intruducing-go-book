package main

import "fmt"

func main() {
	/*Ejercicio 2 */
	fmt.Println(half(1))
	fmt.Println(half(2))	
}

func half(n int) (n1 int, b bool){
	return n%2, n%2 == 0
}