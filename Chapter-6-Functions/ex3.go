package main

import "fmt"

func main() {
	/*Ejemplo de la sentencia if con numeros pares e impares
	usando la sentencia for para recorrer los numeros del 1 al 10
	*/
	fmt.Println(greater(5,3,4,8,1,9,2))	
}

func greater(n ...int) int {
	max := 0
	for i:=0 ; i < len(n) ; i++ {
		if n[i] > max {
			max = n[i]
		}
	} 
	return max
}