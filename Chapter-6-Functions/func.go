package main

import "fmt"

func main() {
	//Ejemplos de funciones 
	/*1. Se saca promedio en una función 
	y retorna el valor rquerido	*/
	xs := []float64{98,93,77,82,83}
	fmt.Println(average(xs))

	/*2.Hace la suma de los numero enteros 
	dados como parametro*/
	fmt.Println(add(1,2,3))

	/*3.Función que saca la mitad de un numero*/
	nextEven := makeEvenGenerator()
	fmt.Println(nextEven()) // 0
	fmt.Println(nextEven()) // 2
	fmt.Println(nextEven()) // 4
}

/*Funcion que retorna el promedio de un array de 
calificaciones*/
func average(xs []float64) float64 {
	total := 0.0
	for _, v := range xs {
		total += v
	}
	return total / float64(len(xs))
}

//Funcion que permite recibir varios enteros
func add(args ...int) int {
	total := 0
	for _, v := range args {
		total += v
	}
	return total
}

func makeEvenGenerator() func() uint {
	i := uint(0)
	return func() (ret uint) {
		ret = i
		i += 2
		return
	}
}