package main

import "fmt"

func main() {
	/*Ejercicio 5 */
	fmt.Println(fib(9))
}

func fib(n int) int{
	if n == 0 {
		return 0
	}else if n == 1 {
		return 1
	}else{
		return fib(n-1) + fib(n-2)
	}
}