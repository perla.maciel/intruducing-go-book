# Capítulo 7. Estructuras e Interfaces

Este capítulo abarca el uso de Struct que es un tipo de dato que contiene nombres de campos utilizando la palabra reservada *type* e Interfaces que al igual que el Sruct utiliza *type* para la declaración y contiene métodos. 

Algunos ejemplos:
* [**Struct** e **Interfaces**](../Chapter-7-Structs-Interfaces/struct-inter.go).

## Ejercicios
### 1. ¿Cuál es la diferencia ente un método y una función?

La diferencia es que un método tiene un receptor y una función no. Además 

La declaració:
* Función
```go 
	func circleArea(c *Circle) float64 {
		return math.Pi * c.r*c.r
	}	
```
* Método
```go 
	func (c *Circle) area() float64 {
		return math.Pi * c.r*c.r
	}	
```
La forma de llamar:
* Función
```go 
	fmt.Println(circleArea(&c))	
```
* Método
```go 
	fmt.Println(c.area())	
```

### 2. ¿Por qué usarias un campo anónimo incrustado en vez de un campo nombrado normal?

Para usar un método directamente como tipo contenedor. 

### 3. Agrega un nuevo parametro en el método 

```go
	type Shape interface {
		perimeter() float64
	}

	func (c *Circle) perimeter() float64 {
		return 2 * math.Pi * c.r
	}

	func (r *Rectangle) perimeter() float64 {
		l := distance(r.x1, r.y1, r.x1, r.y2)
		w := distance(r.x1, r.y1, r.x2, r.y1)
		return 2 * (l + w)
	}

```
Ver [código](../Chapter-7-Structs-Interfaces/ex3.go).


[ :arrow_backward: Capítulo Anterior](/Chapter-6-Functions/chapter-six.md) - [Capítulo Siguiente :arrow_forward: ](/Chapter-8-Packages/chapter-eight.md)
