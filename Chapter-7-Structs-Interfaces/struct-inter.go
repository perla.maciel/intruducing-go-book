package main

import "fmt"

type Shape interface {
	area() float64
}

type MultiShape struct {
	shapes []Shape
}

func main(){
	multiShape := MultiShape{
		shapes: []Shape{
			Circle{0, 0, 5},
			Rectangle{0, 0, 10, 10},
		},
	}
}

func (m *MultiShape) area() float64 {
	var area float64
	for _, s := range m.shapes {
		area += s.area()
	}
	return area
}